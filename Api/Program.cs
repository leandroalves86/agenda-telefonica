using Api.Data;
using Api.Data;
using Api.Validations;
using Microsoft.EntityFrameworkCore;

var builder = WebApplication.CreateBuilder(args);

builder.Services.AddDbContext<AgendaContext>(opt =>
    opt.UseSqlServer(builder.Configuration.GetConnectionString("Sqlserver")));

var defaulPolicyCore = "AllowAll";
builder.Services.AddCors(policy =>
{
    policy.AddPolicy(name: defaulPolicyCore, builder =>{
        builder.AllowAnyOrigin();
        builder.AllowAnyHeader();
        builder.AllowAnyMethod();        
    });
});

builder.Services.AddControllers();
builder.Services.AddEndpointsApiExplorer();
builder.Services.AddSwaggerGen();

builder.Services.AddScoped<IContatosRepository, ContatosRepository>();
builder.Services.AddScoped<IContatosValidator, ContatosValidator>();
builder.Services.AddScoped<ITelefonesRepository, TelefonesRepository>();

var app = builder.Build();

app.UseSwagger();
app.UseSwaggerUI();
app.UseHttpsRedirection();
app.UseCors(defaulPolicyCore);
app.UseAuthorization();
app.MapControllers();

app.Run();
