using Api.Models;
namespace Api.Validations
{

    public class ContatosValidator : IContatosValidator
    {
        public ContatosValidator()
        {
            Erros = new List<Erro>();
        }

        public IList<Erro> Erros { get; }

        public bool Valido => Erros.Count == 0;
        public bool Invalido => !Valido;

        public void Validar(Contato contato)
        {

            if (string.IsNullOrEmpty(contato.Nome))
                Erros.Add(new Erro("O campo nome é obrigatório"));

            if (contato.Nome?.Length < 3)
                Erros.Add(new Erro("O campo nome deve ter pelo menos 3 caracteres"));

            if (contato.Nome?.Length > 50)
                Erros.Add(new Erro("O campo nome deve ter até 50 caracteres"));

            if (contato.Telefones?.Count == 0)
            {
                Erros.Add(new Erro("Informe pelo menos um telefone"));
            }
            else
            {
                foreach (var telefone in contato?.Telefones)
                {
                    if (telefone.DDD is null || telefone.DDD?.ToString().Length != 2)
                        Erros.Add(new Erro($"O campo DDD deve ser informado com até 2 caracteres"));

                    if (telefone.Numero is null || (telefone.Numero?.Length != 8 && telefone.Numero?.Length != 9))
                    {
                        var mensagemErro = "O campo numero deve ter caracteres 8 caracteres para telefone fixo" +
                                            " ou 9 caracteres para telefone celular";
                        Erros.Add(new Erro(mensagemErro));
                    }
                }
            }
        }
    }
}