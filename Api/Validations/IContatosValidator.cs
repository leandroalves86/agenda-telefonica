using Api.Models;

namespace Api.Validations
{
    public interface IContatosValidator
    {
        IList<Erro> Erros { get; }
        bool Valido { get; }
        bool Invalido { get; }

        void Validar(Contato contato);
    }
}