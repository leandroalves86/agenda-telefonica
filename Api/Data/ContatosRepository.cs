using Api.Models;
using Microsoft.EntityFrameworkCore;

namespace Api.Data
{

    public class ContatosRepository : IContatosRepository
    {
        private readonly AgendaContext _context;

        public ContatosRepository(AgendaContext context)
        {
            _context = context;
        }

        public async Task<IEnumerable<Contato>> Todos()
        {
            return await _context.Contatos
                .AsNoTracking()
                .Include(c => c.Telefones)
                .ToListAsync();
        }

        public async Task<Contato> ObterPorId(int id)
        {
            return await _context.Contatos
                .AsNoTracking()
                .Include(c => c.Telefones)
                .FirstOrDefaultAsync(c => c.Id == id);
        }

        public async Task Incluir(Contato contato)
        {
            _context.Contatos.Add(contato);
            await _context.SaveChangesAsync();
        }

        public async Task Atualizar(Contato contato)
        {
            _context.Contatos.Update(contato);
            await _context.SaveChangesAsync();
        }

        public async Task Excluir(Contato contato)
        {
            _context.Contatos.Remove(contato);
            await _context.SaveChangesAsync();
        }
    }
}