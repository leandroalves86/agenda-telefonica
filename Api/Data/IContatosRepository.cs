using Api.Models;

namespace Api.Data
{
    public interface IContatosRepository
    {
        Task<IEnumerable<Contato>> Todos();
        Task<Contato> ObterPorId(int id);
        Task Incluir(Contato contato);
        Task Atualizar(Contato contato);
        Task Excluir(Contato contato);
    }
}