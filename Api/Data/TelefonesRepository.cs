using Api.Data;
using Api.Models;

namespace Api.Data
{
    public class TelefonesRepository : ITelefonesRepository
    {
        private readonly AgendaContext _context;

        public TelefonesRepository(AgendaContext context)
        {
            _context = context;
        }

        public async Task ExcluirTelefones(IEnumerable<Telefone> telefones)
        {
            _context.Telefones.RemoveRange(telefones);
            await _context.SaveChangesAsync();
        }
    }
}