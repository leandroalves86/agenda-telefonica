using Api.Models;

namespace Api.Data
{
    public interface ITelefonesRepository
    {
        Task ExcluirTelefones(IEnumerable<Telefone> telefones);
    }
}