using Api.Models;
using Microsoft.EntityFrameworkCore;

namespace Api.Data
{
    public class AgendaContext : DbContext
    {
        public AgendaContext(DbContextOptions options): base(options){}

        public DbSet<Contato> Contatos { get; set; }
        public DbSet<Telefone> Telefones { get; set; }
    }
}