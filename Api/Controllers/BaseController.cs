using Api.Models;
using Api.Validations;
using Microsoft.AspNetCore.Mvc;

namespace Api.Controllers
{
    public class BaseController : Controller
    {
        [NonAction]
        protected OkObjectResult OkData(Contato contato = default)
        {
            return Ok(new { data = contato, sucesso = true, statusCode = 200 });
        }

        [NonAction]
        protected OkObjectResult OkData(IEnumerable<Contato> contatos)
        {
            return Ok(new { data = contatos, sucesso = true, statusCode = 200 });
        }

        [NonAction]
        protected BadRequestObjectResult ErroBadRequest(Erro erro)
        {
            var erros = new List<Erro>(1);
            erros.Add(erro);
            return ErroBadRequest(erros);
        }

        [NonAction]
        protected BadRequestObjectResult ErroBadRequest(IEnumerable<Erro> erros)
        {
            return BadRequest(new { erros, sucesso = false, statusCode = 400 });
        }
    }
}