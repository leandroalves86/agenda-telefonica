using Api.Data;
using Api.Models;
using Api.Validations;
using Microsoft.AspNetCore.Mvc;

namespace Api.Controllers
{
    [ApiController]
    [Route("api/contatos")]
    public class ContatosController : BaseController
    {
        private readonly IContatosValidator _validator;
        private readonly IContatosRepository _contatos;
        private readonly ITelefonesRepository _telefones;

        public ContatosController(IContatosValidator validator,
                                  IContatosRepository contatosRepository,
                                  ITelefonesRepository telefonesRepository)
        {
            _validator = validator;
            _contatos = contatosRepository;
            _telefones = telefonesRepository;
        }

        [HttpGet]
        public async Task<IActionResult> Get()
        {
            var contatos = await _contatos.Todos();
            if (contatos == null || contatos?.Count() == 0)
                return NotFound();

            return OkData(contatos);
        }

        [HttpGet("{id:int}")]
        public async Task<IActionResult> Get(int id)
        {
            var contato = await _contatos.ObterPorId(id);
            if (contato == null)
                return NotFound();

            return OkData(contato);
        }

        [HttpPost]
        public async Task<IActionResult> Post([FromBody] Contato contato)
        {
            _validator.Validar(contato);
            if (_validator.Invalido)
                return ErroBadRequest(_validator.Erros);

            await _contatos.Incluir(contato);
            return OkData(contato);
        }

        [HttpPut("{id:int}")]
        public async Task<IActionResult> Put(int id, [FromBody] Contato contato)
        {
            if (id != contato.Id)
                return ErroBadRequest(new Erro("O id da rota deve ser igual ao do body"));

            _validator.Validar(contato);
            if (_validator.Invalido)
                return ErroBadRequest(_validator.Erros);

            var contatoDb = await _contatos.ObterPorId(id);
            if (contatoDb == null)
                return ErroBadRequest(new Erro("Nenhum contato foi localizado com este id"));

            var idsTelefones = from tel in contato.Telefones select tel.Id;            
            var telefonesParaExcluir = contatoDb.Telefones.Where(t => !idsTelefones.Contains(t.Id));            
            if(telefonesParaExcluir?.Count() > 0)
                await _telefones.ExcluirTelefones(telefonesParaExcluir);

            await _contatos.Atualizar(contato);
            return OkData(contato);
        }

        [HttpDelete("{id:int}")]
        public async Task<IActionResult> Delete(int id)
        {
            var contato = await _contatos.ObterPorId(id);
            if (contato == null)
                return ErroBadRequest(new Erro("Nenhum contato foi localizado com este id"));

            await _contatos.Excluir(contato);
            return OkData();
        }
    }
}