namespace Api.Models
{
    public class Contato
    {
        //EF
        public Contato() { }
        public Contato(string nome)
        {
            Nome = nome;
            Telefones = new List<Telefone>();
        }

        public Contato(int id, string nome) : this(nome)
        {
            Id = id;
        }

        public int Id { get; set; }
        public string Nome { get; set; }
        public IList<Telefone> Telefones { get; set; }
    }
}