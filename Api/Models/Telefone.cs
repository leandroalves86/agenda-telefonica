using System.Text.Json.Serialization;

namespace Api.Models
{
    public class Telefone
    {
        //EF
        public Telefone(){}

        public Telefone(int? ddd, string numero)
        {
            DDD = ddd;
            Numero = numero;
        }

        public Telefone(int contatoId, int? ddd, string numero)
        {
            ContatoId = contatoId;
            DDD = ddd;
            Numero = numero;
        }

        public int Id { get; set; }
        
        [JsonIgnore]
        public int ContatoId { get; set; }
        public int? DDD { get; set; }
        public string Numero { get; set; }
    }
}