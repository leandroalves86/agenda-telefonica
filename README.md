# Agenda Telefonica
## Projeto de estudos para iniciantes
O objetivo deste projeto é prover uma api simples para ser consumida por estudantes de front end.

## Banco de dados
Para gerar um novo script do banco de dados utilize o comando abaixo:
```sh
dotnet ef migrations script --project Api/AgendaTelefonica.csproj --output ./database.sql
```