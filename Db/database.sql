﻿IF OBJECT_ID(N'[__EFMigrationsHistory]') IS NULL
BEGIN
    CREATE TABLE [__EFMigrationsHistory] (
        [MigrationId] nvarchar(150) NOT NULL,
        [ProductVersion] nvarchar(32) NOT NULL,
        CONSTRAINT [PK___EFMigrationsHistory] PRIMARY KEY ([MigrationId])
    );
END;
GO

BEGIN TRANSACTION;
GO

CREATE TABLE [Contatos] (
    [Id] int NOT NULL IDENTITY,
    [Nome] nvarchar(max) NOT NULL,
    CONSTRAINT [PK_Contatos] PRIMARY KEY ([Id])
);
GO

CREATE TABLE [Telefones] (
    [Id] int NOT NULL IDENTITY,
    [ContatoId] int NOT NULL,
    [DDD] int NULL,
    [Numero] nvarchar(max) NOT NULL,
    CONSTRAINT [PK_Telefones] PRIMARY KEY ([Id]),
    CONSTRAINT [FK_Telefones_Contatos_ContatoId] FOREIGN KEY ([ContatoId]) REFERENCES [Contatos] ([Id]) ON DELETE CASCADE
);
GO

CREATE INDEX [IX_Telefones_ContatoId] ON [Telefones] ([ContatoId]);
GO

INSERT INTO [__EFMigrationsHistory] ([MigrationId], [ProductVersion])
VALUES (N'20221112143240_InitDb', N'7.0.0');
GO

COMMIT;
GO

