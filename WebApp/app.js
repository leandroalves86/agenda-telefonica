const tempTelefones = [];

function load() {
    carregarContatos();
    configForm();
}

function carregarContatos() {
    limparListaContatos();
    obterTodos().then(response => {
        response.data.forEach(contato => {
            incluirContatoLista(contato);
        });
    });
}

function incluirTelefone() {
    if (ddd?.value && numero?.value) {
        let naoExiste = tempTelefones.findIndex(t => t.ddd === ddd.value && t.numero === numero.value) === -1;
        if (naoExiste) {
            const telefone = { ddd: ddd?.value, numero: numero?.value };
            incluirViewTelefone(telefone);
            incluirTempMemoryTelefone(telefone);
            limparCamposFormTelefone();
        }
        else {
            toastAviso('O telefone já está na lista');
        }
    } else {
        toastAviso('Informe o DDD e o número');
    }
}

function incluirTempMemoryTelefone(telefone) {
    tempTelefones.push(telefone);
}

function removerTelefone(telefone) {
    removerTempTelefone(telefone.parentElement);
    removerTelefoneFormLista(telefone);
}

function removerTempTelefone(telefone) {
    let text = telefone.children[1]?.children[0]?.innerText;
    text = text.replace(/[^\d+]/g, '');
    var obj = { ddd: text.substr(0, 2), numero: text.substr(2) };
    var indice = tempTelefones.findIndex(t => t.ddd == obj.ddd && t.numero == obj.numero);
    tempTelefones.splice(indice, 1);
}

function salvar() {
    limparErros();
    if (!idContato.value) {
        IncluirContato();
    } else {
        AtualizarContato();
    }
}

async function IncluirContato() {
    const { sucesso, data, erros } = await incluir({ nome: nomeContato?.value, telefones: tempTelefones });
    if (sucesso) {
        incluirContatoLista(data);
        limparForm();
        toastSucesso('Cadastrado com sucesso!');
    }
    else {
        toastErro('Ocorreram erros ao tentar cadastrar!');
        carregarErros(erros);
    }
}

async function AtualizarContato() {
    const { sucesso, data, erros } = await atualizar({ id: idContato?.value, nome: nomeContato?.value, telefones: tempTelefones });
    if (sucesso) {
        atualizarContatoLista(data);
        limparForm();
        toastSucesso('Atualizado com sucesso!');
    }
    else {
        toastErro('Ocorreram erros ao tentar atualizar!');
        carregarErros(erros);
    }
}

async function excluirContato(contato) {
    limparForm();
    const id = contato.parentElement?.children[0]?.children[0]?.innerText;
    const nome = contato.parentElement?.children[0]?.children[1]?.innerText;
    const { isConfirmed } = await confirmar(`Deseja realamente excluir o contato: "${nome}"?`);
    if (isConfirmed) {
        const { sucesso, erros } = await excluir(id);
        if (sucesso) {
            contato.parentElement.remove();
            toastSucesso("Contato excluído com sucesso");
        }
        else {
            toastErro('Ocorreu um erro ao excluir o contato');
            carregarErros(erros);
        }
    }
}