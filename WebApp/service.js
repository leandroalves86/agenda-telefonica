//referência: https://www.bezkoder.com/javascript-fetch/

const base_url = 'http://localhost:5160/api';
const headers = { "Content-Type": "application/json" };

async function obterTodos() {
    const response = await fetch(`${base_url}/contatos`, { headers });
    return await response.json();
}

async function obterPorId(id) {
    const response = await fetch(`${base_url}/contatos/${id}`, { headers });
    return await response.json();
}

async function incluir(contato) {
    const options = { headers, method: 'post', body: JSON.stringify(contato) };
    const response = await fetch(`${base_url}/contatos`, options);
    return await response.json();
}

async function atualizar(contato) {
    const options = { headers, method: 'put', body: JSON.stringify(contato) };
    const response = await fetch(`${base_url}/contatos/${contato.id}`, options);
    return await response.json();
}

async function excluir(id) {
    const options = { headers, method: 'delete'};
    const response = await fetch(`${base_url}/contatos/${id}`, options);
    return await response.json();
}