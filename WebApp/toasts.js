const configToast = {
    toast: true,
    position: 'top-end',
    showConfirmButton: false,
    timer: 3000,
    timerProgressBar: true,
    didOpen: (toast) => {
        toast.addEventListener('mouseenter', Swal.stopTimer)
        toast.addEventListener('mouseleave', Swal.resumeTimer)
    }
};

function toastSucesso(mensagem) {
    Swal.mixin(configToast).fire({
        icon: 'success',
        title: mensagem,
    });
}

function toastErro(mensagem) {
    Swal.mixin(configToast).fire({
        icon: 'error',
        title: mensagem,
    });
}

function toastAviso(mensagem) {
    Swal.mixin(configToast).fire({
        icon: 'warning',
        title: mensagem,
    });
}

async function confirmar(mensagem) {
    return await Swal.fire({
        title: 'Confirmação!',
        text: mensagem,
        icon: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#d33',
        cancelButtonColor: '#3085d6',
        confirmButtonText: 'Excluir',
        cancelButtonText: 'Cancelar'
    });
}