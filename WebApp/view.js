const contatos = document.getElementById('contatos');
const idContato = document.getElementById('id-contato');
const nomeContato = document.getElementById('nome-contato');
const telefones = document.getElementById('lst-telefones-form');
const ddd = document.getElementById('telefone-ddd');
const numero = document.getElementById('telefone-numero');
const frmContato = document.getElementById('frm-contato');
const toast = document.getElementById('toast');
const errosAlert = document.querySelector('.alert ol');

const templateTelefone =
    `
    <li class="list-group-item list-group-item-action d-flex justify-content-between align-items-start">
        <small class="d-none">@id</small>
        <div class="ms-2 me-auto">
            <h6 class="mb-1">(@ddd) @numero</h5>                        
        </div>
        <button type="button" class="btn btn-sm btn-outline-danger" onclick="removerTelefone(this)">Remover</button>
    </li>
    `;

const templateContato =
    `
    <li class="list-group-item list-group-item-action d-flex justify-content-between align-items-start">
            <div class="ms-2 me-auto">            
            <small>@id</small>
            <h5 class="mb-1">@nome</h5>
            <div>@telefone</div>            
            
            </div>
            <button type="button" class="btn btn-info btn-sm m-1" onclick="carregarEditarContatoForm(this)">Editar</button>
            <button type="button" class="btn btn-danger btn-sm m-1" onclick="excluirContato(this)">Excluir</button>
    </li>
    `;

const templateAlertErro = `<li class="list-group-item list-group-item-danger">@mensagem</li>`;

function configForm(){
    frmContato.addEventListener('submit', function (e) {
        e.preventDefault();
    });
}

function incluirContatoLista(contato) {
    let item = templateContato
        .replace('@nome', contato.nome)
        .replace('@telefone', `(${contato.telefones[0]?.ddd}) ${contato.telefones[0]?.numero}`)
        .replace('@id', contato.id);
    contatos.insertAdjacentHTML('beforeend', item);
}

function atualizarContatoLista(contato) {
    let oldElement = Array.from(contatos.children).find(e => e.children[0].children[0].innerText == contato.id);
    oldElement.remove();
    let item = templateContato
        .replace('@nome', contato.nome)
        .replace('@telefone', `(${contato.telefones[0]?.ddd}) ${contato.telefones[0]?.numero}`)
        .replace('@id', contato.id);
    contatos.insertAdjacentHTML('beforeend', item);
}

function incluirViewTelefone(telefone) {
    let template = templateTelefone
        .replace('@ddd', telefone.ddd)
        .replace('@numero', telefone.numero)
        .replace('@id', telefone?.id ? telefone.id : '');
    telefones.insertAdjacentHTML('afterbegin', template);
}

async function carregarEditarContatoForm(contato) {
    limparForm();
    bloquearListaContatos();
    const id = contato.parentElement?.children[0]?.children[0]?.innerText;
    const { data, sucesso, erros } = await obterPorId(id);
    if (sucesso) {
        nomeContato.value = data.nome;
        idContato.value = data.id;
        data.telefones.forEach(telefone => {
            incluirViewTelefone(telefone);
            incluirTempMemoryTelefone(telefone);
        });
    }
    else {
        toastErro('Ocorreu um erro ao obter dados do contato');
        carregarErros(erros);
    }
}

function carregarErros(erros) {
    limparErros();
    erros.forEach(erro => errosAlert
        .insertAdjacentHTML('afterbegin', templateAlertErro.replace('@mensagem', erro?.mensagem)));
    errosAlert.parentElement.classList.remove('d-none');
}

function limparErros() {
    errosAlert.innerHTML = '';
    errosAlert.parentElement.classList.add('d-none');
}

function limparForm() {
    nomeContato.value = '';
    telefones.innerHTML = '';
    tempTelefones.length = 0;
    idContato.value = '';
    limparCamposFormTelefone();
    limparErros();
    desbloquearListaContatos();
}

function limparCamposFormTelefone() {
    ddd.value = '';
    numero.value = '';
}

function removerTelefoneFormLista(telefone)
{
    telefone.parentElement.remove();
}

function limparListaContatos(){
    contatos.innerHTML = '';
}

function bloquearListaContatos() {
    Array.from(contatos.children)
        .forEach(element => element.classList.add('disabled'));
}

function desbloquearListaContatos() {
    Array.from(contatos.children)
        .forEach(element => element.classList.remove('disabled'));
}