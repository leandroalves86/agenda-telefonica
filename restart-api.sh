#!/bin/bash
SERVICE_NAME=api

docker-compose stop $SERVICE_NAME
docker-compose rm -f $SERVICE_NAME
docker-compose build
docker-compose up -d
docker-compose logs -f --tail="all" $SERVICE_NAME